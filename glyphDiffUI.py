from importlib import reload
import htmlDiff
reload(htmlDiff)

import AppKit
import vanilla
import difflib
from math import cos, sin, radians
import mojo.drawingTools as ctx
from fontTools.misc.arrayTools import unionRect
from fontTools.ufoLib.glifLib import writeGlyphToString
from mojo.UI import CodeEditor, HTMLView, getDefault
from mojo.canvas import CanvasGroup
from mojo.events import addObserver, removeObserver
from colorsys import hls_to_rgb
from htmlDiff import makeHTML

def triangle(pos, radius, angle=90):
    x, y = pos
    x0 = x + cos(radians(angle)) * radius
    y0 = y + sin(radians(angle)) * radius
    angle += 120
    x1 = x + cos(radians(angle)) * radius
    y1 = y + sin(radians(angle)) * radius
    angle += 120
    x2 = x + cos(radians(angle)) * radius
    y2 = y + sin(radians(angle)) * radius
    ctx.polygon((x0, y0), (x1, y1), (x2, y2))

class ViewCollector(object):
    pass

class GlyphDiff(vanilla.Group):

    color1  = hls_to_rgb(0, 0.5, 1.0)
    color2  = hls_to_rgb(0.333, 0.4, 1.0)

    def __init__(self, posSize, glyph1, glyph2, mode="visual"):
        super().__init__(posSize)

        self.tabs = vanilla.Tabs(
                (0, 0, -0, -25),
                ["xml", "visual"],
                showTabs=False)

        self.switcher = vanilla.RadioGroup(
                (7, -23, 120, 20),
                ["xml", 'visual'],
                isVertical=False,
                callback=self.switcherCallback,
                sizeStyle="small")
        
        self.loadGlyphs(glyph1, glyph2)
        
        if mode == "visual":
            mode = 1
        elif mode == "xml":
            mode = 0
        else:
            mode = None
        if mode is not None:
            self.switcher.set(mode)
            self.tabs.set(mode)
            
      
    @property
    def backgroundColor(self):
        return getDefault("glyphViewMarginColor")

    @property
    def onCurvePointsSize(self):
        return getDefault("glyphViewOncurvePointsSize")

    @property
    def offCurvePointsSize(self):
        return getDefault("glyphViewOffCurvePointsSize")

    @property
    def glyphViewStrokeColor(self):
        return getDefault("glyphViewStrokeColor", defaultValue=(0, 0.2))

    @property
    def glyphViewStrokeWidth(self):
        return getDefault("glyphViewStrokeWidth", defaultValue=1)

    @property
    def glyphViewMetricsColor(self):
        return getDefault("glyphViewMetricsColor", defaultValue=1)

    @property
    def textFontSize(self):
        return getDefault("textFontSize")

    def _getBounds(self):
        self.bounds = None
        if self.glyph1.naked().controlPointBounds:
            self.bounds = self.glyph1.naked().controlPointBounds
            if self.glyph2.naked().controlPointBounds:
                self.bounds = unionRect(self.bounds, self.glyph2.naked().controlPointBounds)
        else:
            self.bounds = self.glyph2.naked().controlPointBounds

    def _getPoints(self):
        self.points1 = set([(p.x, p.y) for c in self.glyph1 for p in c.points])
        self.points2 = set([(p.x, p.y) for c in self.glyph2 for p in c.points])
        self.pointsShared = self.points1 & self.points2
        # self.pointsChanged = self.points1 ^ self.points2

    def _getSegments(self):
        self.segments1 = set([tuple([(p.x, p.y) for p in s]) for c in self.glyph1 for s in c])
        self.segments2 = set([tuple([(p.x, p.y) for p in s]) for c in self.glyph2 for s in c])
        self.segmentsShared = self.segments1 & self.segments2
        # self.segmentsChanged = self.segments1 ^ self.segments2

    def _getAnchors(self):
        self.anchors1 = set([(a.x, a.y) for a in self.glyph1.anchors])
        self.anchors2 = set([(a.x, a.y) for a in self.glyph2.anchors])
        self.anchorsShared = self.anchors1 & self.anchors2
        # self.anchorsChanged = self.anchors1 ^ self.anchors2

    def _getVMetrics(self):
        vMetrics = ['descender', 'xHeight', 'ascender', 'capHeight']
        self.vMetrics1 = set([getattr(self.glyph1.font.info, v) for v in vMetrics]+[0])
        self.vMetrics2 = set([getattr(self.glyph2.font.info, v) for v in vMetrics]+[0])
        self.vMetricsShared = self.vMetrics1 & self.vMetrics2

    def _getAreas(self):
        try:
            self.sharedArea = self.glyph1 & self.glyph2
        except:
            self.sharedArea = None
        try:
            self.area1 = self.glyph1 % self.glyph2
        except:
            self.area1 = None
        try:
            self.area2 = self.glyph2 % self.glyph1
        except:
            self.area2 = None

    def _getXML(self):
        self.xml1 = writeGlyphToString(self.glyph1.name, glyphObject=self.glyph1.naked(), drawPointsFunc=self.glyph1.drawPoints) # formatVersion=self.glyph1.naked().font.ufoFormatVersionTuple
        self.xml2 = writeGlyphToString(self.glyph2.name, glyphObject=self.glyph2.naked(), drawPointsFunc=self.glyph2.drawPoints) # formatVersion=self.glyph2.naked().font.ufoFormatVersionTuple

    def _getDiff(self):
        self.diffHTML = makeHTML(self.xml1, self.xml2)

    def loadGlyphs(self, glyph1, glyph2):
        self.glyph1, self.glyph2 = glyph1, glyph2

        self._getBounds()
        self._getPoints()
        self._getSegments()
        self._getAnchors()
        self._getAreas()
        self._getVMetrics()
        self._getXML()
        self._getDiff()

        self.views = ViewCollector()
        self.views.drawing = CanvasGroup((0, 0, -0, -0), delegate=self)
        self.views.html = HTMLView((0, 0, -0, -0))
        self.views.html.setHTML(self.diffHTML)

        if hasattr(self.tabs[0], 'html'):
            del self.tabs[0].html
        if hasattr(self.tabs[1], 'drawing'):
            del self.tabs[1].drawing

        self.tabs[0].html = self.views.html
        self.tabs[1].drawing = self.views.drawing

    def switcherCallback(self, sender):
        self.tabs.set(sender.get())

    def shouldDrawBackground(self):
        return True # False

    def draw(self):

        glyphs   = [self.glyph1, self.glyph2]
        areas    = [self.area1, self.area2]
        colors   = [self.color1, self.color2]
        anchors  = [self.anchors1, self.anchors2]
        vMetrics = [self.vMetrics1, self.vMetrics2]

        padding = 20

        viewWidth = self.views.drawing.width()
        viewHeight = self.views.drawing.height()

        viewAreaWidth = viewWidth - padding * 2
        viewAreaHeight = viewHeight - padding * 2

        w = max([g.width for g in glyphs])
        h = max([g.font.info.unitsPerEm for g in glyphs])
        d = max([g.font.info.descender for g in glyphs])

        scaleValueX = viewAreaWidth / w
        scaleValueY = viewAreaHeight / h

        scaleValue = scaleValueX if scaleValueX < scaleValueY else scaleValueY

        shiftX = (viewAreaWidth / scaleValue - w) * 0.5
        shiftY = (viewAreaHeight / scaleValue - h) * 0.5

        inverseScale = 1.0 / scaleValue

        xLeft = padding + shiftX * scaleValue
        yBottom = 0
        yTop = padding + viewAreaHeight * inverseScale

        # draw background
        ctx.fill(*self.backgroundColor)
        ctx.rect(0, 0, shiftX * scaleValue + padding, viewHeight)
        ctx.rect(viewWidth - shiftX * scaleValue - padding, 0, shiftX * scaleValue + padding, viewHeight)

        ctx.save()
        ctx.strokeWidth(1)

        # draw vertical metrics
        for i, color in enumerate(colors):
            color += (0.8,)
            yValues = list(vMetrics[i])
            for yValue in yValues:
                if not yValue in self.vMetricsShared:
                    ctx.stroke(*color)
                else:
                    if i > 0:
                        continue
                    ctx.stroke(*self.glyphViewMetricsColor)
                yValue += (shiftY - d)
                yValue *= scaleValue
                yValue += padding
                yValue = int(yValue)
                ctx.line((0, yValue), (viewWidth, yValue))

        # draw margins
        ctx.lineDash(2, 1)
        if self.glyph1.width != self.glyph2.width:
            for i, color in enumerate(colors):
                color += (0.8,)
                glyph = glyphs[i]
                xRight = xLeft + glyph.width * scaleValue
                ctx.stroke(*color)
                ctx.line((xRight, yBottom), (xRight, yTop))

        ctx.restore()

        # setup glyph position & scale
        ctx.translate(padding, padding)
        ctx.scale(scaleValue)
        ctx.translate(shiftX, shiftY - d)

        # draw shared area
        # if self.sharedArea:
        #     ctx.stroke(None)
        #     ctx.fill(0, 0.1)
        #     ctx.drawGlyph(self.sharedArea)

        # draw areas
        for i, color in enumerate(colors):
            glyph = areas[i]
            if not glyph:
                continue
            color += (0.2,)
            ctx.stroke(None)
            ctx.fill(*color)
            ctx.drawPath(glyph.getRepresentation("defconAppKit.NoComponentsNSBezierPath"))

        # draw segments
        for i, color in enumerate(colors):
            glyph = glyphs[i]
            color += (0.8,)
            ctx.fill(None)
            ctx.strokeWidth(self.glyphViewStrokeWidth * inverseScale)

            for contour in glyph:
                for si, segment in enumerate(contour):

                    segmentPoints = tuple([(p.x, p.y) for p in segment])
                    p0 = contour[(si - 1) % len(contour)][-1]
                    p0 = p0.x, p0.y

                    ctx.newPath()
                    ctx.moveTo(p0)

                    if len(segmentPoints) == 3:
                        ctx.curveTo(*segmentPoints)
                    else:
                        ctx.lineTo(segmentPoints[0])

                    if not segmentPoints in self.segmentsShared:
                        ctx.stroke(*color)
                    else:
                        if i > 0:
                            continue
                        ctx.stroke(*self.glyphViewStrokeColor)

                    ctx.drawPath()

        # draw components
        for i, color in enumerate(colors):
            g = glyphs[i]
            color += (0.2,)
            ctx.stroke(None)
            ctx.fill(*color)
            ctx.drawPath(g.getRepresentation("defconAppKit.OnlyComponentsNSBezierPath"))

        # draw points
        r1 = self.onCurvePointsSize * inverseScale
        r2 = self.offCurvePointsSize * inverseScale
        for i, color in enumerate(colors):
            colorHandles = color + (0.3,)
            glyph = glyphs[i]
            ctx.fill(None)
            ctx.strokeWidth(1 * inverseScale)
            ctx.stroke(*color)
            for contour in glyph:
                for si, segment in enumerate(contour):
                    p0 = contour[(si - 1) % len(contour)][-1]
                    if len(segment) == 3:
                        p1, p2, p3 = segment
                    else:
                        p1 = p2 = None
                        p3 = segment[0]
                    # on-curve
                    if (p3.x, p3.y) not in self.pointsShared:
                        if p3.type == 'line':
                            if p3.smooth:
                                triangle((p3.x, p3.y), r1 * 1.2)
                            else:
                                ctx.rect(p3.x - r1, p3.y - r1, r1 * 2, r1 * 2)
                        else:
                            ctx.oval(p3.x - r1, p3.y - r1, r1 * 2, r1 * 2)
                    # off-curve
                    if p1:
                        if (p1.x, p1.y) not in self.pointsShared:
                            ctx.stroke(*colorHandles)
                            ctx.line((p1.x, p1.y), (p0.x, p0.y))
                            ctx.stroke(*color)
                            ctx.oval(p1.x - r2, p1.y - r2, r2 * 2, r2 * 2)
                    if p2:
                        if (p2.x, p2.y) not in self.pointsShared:
                            ctx.stroke(*colorHandles)
                            ctx.line((p2.x, p2.y), (p3.x, p3.y))
                            ctx.stroke(*color)
                            ctx.oval(p2.x - r2, p2.y - r2, r2 * 2, r2 * 2)

        # draw anchors
        for i, color in enumerate(colors):
            glyph = glyphs[i]
            ctx.stroke(None)
            ctx.fill(*color)
            for anchor in glyph.anchors:
                if (anchor.x, anchor.y) not in self.anchorsShared:
                    ctx.oval(anchor.x - r2, anchor.y - r2, r2 * 2, r2 * 2)
                    # ctx.fontSize(self.textFontSize * inverseScale)
                    # ctx.text(anchor.name, (anchor.x - dx, anchor.y - self.textFontSize * inverseScale * 2))

# -------
# testing
# -------

if __name__ == '__main__':

    from defconAppKit.windows.baseWindow import BaseWindowController

    class GlyphDiffTester(BaseWindowController):

        def __init__(self, glyph):
            if glyph is None:
                print('select a glyph!')
                return
            self.glyph = glyph
            self.w = vanilla.Window((600, 600), title='GlyphDiff', minSize=(300, 300))
            self.w.diff = GlyphDiff((0, 0, -0, -50), self.glyph1, self.glyph2)
            self.w.diffButton = vanilla.Button(
                    (-110, -30, 100, 22),
                    "popup",
                    sizeStyle='small',
                    callback=self.diffButtonCallback)
            self.setUpBaseWindowBehavior()
            addObserver(self, "updateDiffs", "currentGlyphChanged")
            self.w.open()

        @property
        def glyph1(self):
            return self.glyph.getLayer('foreground')

        @property
        def glyph2(self):
            return self.glyph.getLayer('sketches')

        def diffButtonCallback(self, sender):
            pop = vanilla.Popover((700, 600))
            pop.diff = GlyphDiff((0, 0, 0, 0), self.glyph1, self.glyph2)
            pop.open(parentView=sender, preferredEdge="bottom")
            sender.___pop = pop

        def updateDiffs(self, notification):
            self.glyph = notification['glyph']
            self.w.diff.loadGlyphs(self.glyph1, self.glyph2)

        def windowCloseCallback(self, sender):
            super().windowCloseCallback(sender)
            removeObserver(self, "currentGlyphChanged")

    GlyphDiffTester(CurrentGlyph())
