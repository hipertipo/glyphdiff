### https://pryp.in/blog/1/diffs-with-syntax-highlight.html
import difflib
from fontTools.ufoLib.glifLib import writeGlyphToString

import pygments
from pygments.lexers import get_lexer_by_name
import pygments.formatters
from pygments.token import STANDARD_TYPES, string_to_tokentype

from lib.scripting.codeEditor import fallbackStyleDict
from lib.tools.defaults import getDefault, getDefaultFont

tokens = dict(fallbackStyleDict)
tokens.update(getDefault("PyDETokenColors", dict()))

# f = getDefaultFont("PyDEFont")

CSS = "pre {font-family:'Menlo'; font-style:roman; white-space:pre; overflow:scroll}\n"

HTML = """
<html>
<head>
<style>
body, html {
    padding: 0;
    margin: 0;
}
pre {
  font-family: monospace;
  font-size: 11px;
  line-height: 1.6em;
  float: left;
  min-width: 100%%;
}
div, .highlight {
  margin: 0;
  padding: 0;
}
.diff_line {
	padding-left: 10px;
}
.diff_minus {
  background-color: rgba(255, 0, 0, 0.1)
}
.diff_plus {
  background-color: rgba(0, 255, 0, 0.1)
}
.diff_special {
  background-color: rgba(128, 128, 128, 0.1)
}
.diff_changed {
  font-style: bold;
}
%(css)s
</style>
</head>
<body>
<pre class="highlight">
"""

for key, value in tokens.items():
    token = string_to_tokentype(key)
    cssClss = STANDARD_TYPES.get(token)
    if cssClss:
        value = value.replace("bold", "font-weight:bold")
        value = value.replace("italic", "font-style:italic")
        value = ";".join(value.split(" "))
        CSS += f".{cssClss} {{color:#{value};}}\n"

class Formatter(pygments.formatters.HtmlFormatter):
    def wrap(self, source, outfile):
        return source

def makeHTML(xml1, xml2):

    colored1 = pygments.highlight(xml1, lexer=get_lexer_by_name("xml"), formatter=Formatter())
    colored2 = pygments.highlight(xml2, lexer=get_lexer_by_name("xml"), formatter=Formatter())

    html = HTML % dict(css=CSS)

    prevClssName = None
    for line in difflib.ndiff(xml1.splitlines(), xml2.splitlines()):
        line = pygments.highlight(line, lexer=get_lexer_by_name("xml"), formatter=Formatter())
        line = line.replace("\n", "")
        clss = []
        clssName = {'+': 'diff_plus', '-': 'diff_minus', '@': 'diff_special', '?': 'diff_changed'}.get(line[:1], '')
        clss.append(clssName)
        if clssName == "diff_changed" and prevClssName:
            clss.append(prevClssName)
        prevClssName = clssName

        line = f'<div class="diff_line {" ".join(clss)}">{line}</div>'
        html += line
    html += "</pre>"
    html += "</body>"
    html += "</html>"

    return html

if __name__ == '__main__':

    import os

    glyph1 = CurrentGlyph()
    glyph2 = glyph1.getLayer("sketches")

    xml1 = writeGlyphToString(glyph1.name, glyphObject=glyph1.naked(), drawPointsFunc=glyph1.drawPoints, formatVersion=glyph1.naked().font.ufoFormatVersion)
    xml2 = writeGlyphToString(glyph2.name, glyphObject=glyph2.naked(), drawPointsFunc=glyph2.drawPoints, formatVersion=glyph2.naked().font.ufoFormatVersion)

    folder = os.getcwd()
    htmlPath = os.path.join(folder, 'diffTest.html')
    html = makeHTML(xml1, xml2)
    with open(htmlPath, 'w') as f:
        f.write(html)
